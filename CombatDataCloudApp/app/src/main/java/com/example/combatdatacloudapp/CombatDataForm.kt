package com.example.combatdatacloudapp

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toDrawable
import androidx.core.graphics.red
import androidx.core.graphics.toColor
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.datastore.*
import kotlinx.android.synthetic.main.activity_combat_data_form.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import yuku.ambilwarna.AmbilWarnaDialog
import java.io.InputStream

class CombatDataForm : AppCompatActivity(),AmbilWarnaDialog.OnAmbilWarnaListener {
    //declaring google datastore variables
    lateinit var MyCurrentCombatData:CombatData
    lateinit var inputStream:InputStream
    lateinit var credentials:GoogleCredentials
    lateinit var datastore: Datastore
    lateinit var taskKey:Key
    var SelectedColor:Int = 0
    //setup the kind for storing the data in the datastore
    val kind = ConstKeywords.ENTITY_KIND

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_combat_data_form)

        //receiving data from main activity to change the title
        val TitleText = intent.getStringExtra(ConstKeywords.FORM_TITLE_KEY)
        supportActionBar?.title = TitleText

        //connecting to google firestore in datastore mode
        MyCurrentCombatData = CombatData(TitleText)
        inputStream = assets.open(ConstKeywords.CREDENTIALS_NAME)
        credentials = GoogleCredentials.fromStream(inputStream)
        datastore = DatastoreOptions.newBuilder().setProjectId(ConstKeywords.PROJECT_ID).setCredentials(credentials).build().service
        //the cloud datastore key for the new entity
        taskKey = datastore.newKeyFactory().setKind(kind).newKey(MyCurrentCombatData.KeyName)


        //run async function to retrieve data from datastore and populate the form.
       GlobalScope.launch(Dispatchers.IO) {
           RetriveDataFromDatastore()
        }
    }

    //run async function to save data to the datastore.
    fun SaveFunction(view:View)
    {
        GlobalScope.launch(Dispatchers.IO) {
            //check if any feild is emty then return to prevent from crash
            if( TextUtils.isEmpty(findViewById<EditText>(R.id.KnockbackDamageMultiplierValue).text.toString()) ||
                TextUtils.isEmpty(findViewById<EditText>(R.id.KnockbackForceValue).text.toString()) ||
                TextUtils.isEmpty(findViewById<EditText>(R.id.KnockupForceValue).text.toString()) ||
                TextUtils.isEmpty(findViewById<EditText>(R.id.BaseDamageMultiplierValue).text.toString()) ||
                TextUtils.isEmpty(findViewById<EditText>(R.id.BleedDamageMultiplierValue).text.toString())||
                TextUtils.isEmpty(findViewById<EditText>(R.id.BleedDebuffValue).text.toString()) ||
                TextUtils.isEmpty(findViewById<EditText>(R.id.BleedDurationValue).text.toString()) ||
                TextUtils.isEmpty(findViewById<EditText>(R.id.PiercingDamageMultiplierValue).text.toString())||
                TextUtils.isEmpty(findViewById<EditText>(R.id.PiercingDamageValue).text.toString())||
                TextUtils.isEmpty(findViewById<EditText>(R.id.StunDamageMultiplierValue).text.toString())||
                TextUtils.isEmpty(findViewById<EditText>(R.id.StundDurationValue).text.toString()))
            {
                runOnUiThread(java.lang.Runnable {
                    Toast.makeText(view.context, "No Empty Fields Allowed!", Toast.LENGTH_SHORT)
                        .show()
                })
                return@launch
            }
            else
            {
                AddDataToDatastore()
                //prints toast to show data has been successfully added.
                runOnUiThread(java.lang.Runnable {
                    Toast.makeText(view.context, "Data Successfully Added ", Toast.LENGTH_SHORT)
                        .show()
                })
            }

        }
    }

    //function to enable color picker
    fun ColorSelect(view: View)
    {
        openColorPicker(false)
    }

    fun openColorPicker(supportAlpha:Boolean)
    {
        val dialogue:AmbilWarnaDialog = AmbilWarnaDialog(this,SelectedColor,supportAlpha,this)
        dialogue.show()
    }

    override fun onCancel(dialog: AmbilWarnaDialog?) {
        // we do not need to implement the cancel method
    }

    override fun onOk(dialog: AmbilWarnaDialog?, color: Int) {
        SelectedColor = color
        //changes the Color of the Color picker button
        SelectedColorView.setBackgroundColor(Color.valueOf(SelectedColor).toArgb())
    }


    //fun to retrieve data from datastore
    fun RetriveDataFromDatastore()
    {
        //Retrieve entity
        val retrieved = datastore.get(taskKey)

        val BasepropertiesData = retrieved.getValue<EntityValue>(ConstKeywords.BASE_ENTITY).get().properties
        val KnockbackpropertiesData = retrieved.getValue<EntityValue>(ConstKeywords.KNOCKBACK_ENTITY).get().properties
        val BleedpropertiesData = retrieved.getValue<EntityValue>(ConstKeywords.BLEED_ENTITY).get().properties
        val PiercingpropertiesData = retrieved.getValue<EntityValue>(ConstKeywords.PIERCING_ENTITY).get().properties
        val StunpropertiesData = retrieved.getValue<EntityValue>(ConstKeywords.STUN_ENTITY).get().properties
        val ColorData = BasepropertiesData.getValue(ConstKeywords.DAMAGE_COLORS).get()
        var colorValues = ColorData as List<DoubleValue>

        //executes this function on Ui thread as only the thread that creates the view is able to update its values.
        runOnUiThread(java.lang.Runnable{

            //stores the base Damage Multiplier value
            BaseDamageMultiplierValue.setText(BasepropertiesData.getValue(ConstKeywords.DAMAGE_MULTIPLIER).get().toString())
            //stores the selected color as int.
            SelectedColor = Color.valueOf(colorValues[0].get().toFloat(),colorValues[1].get().toFloat(),colorValues[2].get().toFloat()).toArgb()


            //Update KnockUp Damage Multiplier
            KnockbackDamageMultiplierValue.setText(KnockbackpropertiesData.getValue(ConstKeywords.DAMAGE_MULTIPLIER).get().toString())
            //Update KnockUp Force
            KnockupForceValue.setText(KnockbackpropertiesData.getValue(ConstKeywords.KNOCKUP_FORCE).get().toString())
            //Update KnockBack Force
            KnockbackForceValue.setText(KnockbackpropertiesData.getValue(ConstKeywords.KNOCKBACK_FORCE).get().toString())

            // updates the color values
            SelectedColorView.setBackgroundColor(SelectedColor)

            //Update Bleed Damage Multiplier
            BleedDamageMultiplierValue.setText(BleedpropertiesData.getValue(ConstKeywords.DAMAGE_MULTIPLIER).get().toString())
            //Update Bleed Debuff
            BleedDebuffValue.setText(BleedpropertiesData.getValue(ConstKeywords.BLEED_DEBUFF).get().toString())
            //Update Bleed Duration
            BleedDurationValue.setText(BleedpropertiesData.getValue(ConstKeywords.BLEED_DURATION).get().toString())

            //Update Piercing Damage Multiplier
            PiercingDamageMultiplierValue.setText(PiercingpropertiesData.getValue(ConstKeywords.DAMAGE_MULTIPLIER).get().toString())
            //Update Piercing Damage
            PiercingDamageValue.setText(PiercingpropertiesData.getValue(ConstKeywords.PIERCING_DAMAGE).get().toString())

            //Update Stun Damage Multiplier
            StunDamageMultiplierValue.setText(StunpropertiesData.getValue(ConstKeywords.DAMAGE_MULTIPLIER).get().toString())
            //Update Stun Duration
            StundDurationValue.setText(StunpropertiesData.getValue(ConstKeywords.STUN_DURATION).get().toString())
        })

    }


    //function to add data to datastore when clicked save
    fun AddDataToDatastore(){

        val currentSelectedColor = Color.valueOf(SelectedColor)

        //sets up Combat Data for Knock Up Type
        MyCurrentCombatData.BaseType = DamageValues(BaseDamageMultiplierValue.text.toString().toDouble(), Color.valueOf(SelectedColor))

        //sets up Combat Data for Knock Up Type
        MyCurrentCombatData.KnockbackType = Knockback(DamageValues(
                                                                    KnockbackDamageMultiplierValue.text.toString().toDouble(),
                                                                    Color.valueOf(SelectedColor)),
                                                                    KnockbackForceValue.text.toString().toDouble(),
                                                                    KnockupForceValue.text.toString().toDouble())


        //sets up Combat Data for Bleed Type
        MyCurrentCombatData.BleedType = Bleed(DamageValues(
            BleedDamageMultiplierValue.text.toString().toDouble(),
            Color.valueOf(SelectedColor)),
            BleedDebuffValue.text.toString().toDouble(),
            BleedDurationValue.text.toString().toDouble())

        //sets up Combat Data for Piercing Type
        MyCurrentCombatData.PiercingType = Piercing(DamageValues(
            PiercingDamageMultiplierValue.text.toString().toDouble(),
            Color.valueOf(SelectedColor)),
            PiercingDamageValue.text.toString().toDouble())

        //sets up Combat Data for Stun Type
        MyCurrentCombatData.StunType = Stun(DamageValues(
            StunDamageMultiplierValue.text.toString().toDouble(),
            Color.valueOf(SelectedColor)),
            StundDurationValue.text.toString().toDouble())



        //set up the Damage Color for the Current Combat Data
        val DamageColor = MyCurrentCombatData.KnockbackType!!.DamaveValues!!.DamageColors
        var ColorList = mutableListOf(
            DoubleValue.newBuilder(DamageColor.red().toDouble()).build(),
            DoubleValue.newBuilder(DamageColor.green().toDouble()).build(),
            DoubleValue.newBuilder(DamageColor.blue().toDouble()).build())

        //set up the basedata for damage types
        val BaseData = Entity.newBuilder(taskKey).set(ConstKeywords.DAMAGE_MULTIPLIER,MyCurrentCombatData.BaseType!!.DamageMultiplier)
            .set(ConstKeywords.DAMAGE_COLORS,ColorList)
            .build()

        //set up the knockbackdata for damage types
        val  KnockbackData =  Entity.newBuilder(taskKey).set(ConstKeywords.DAMAGE_MULTIPLIER,MyCurrentCombatData.KnockbackType!!.DamaveValues!!.DamageMultiplier)
            .set(ConstKeywords.KNOCKBACK_FORCE,MyCurrentCombatData.KnockbackType!!.knockBackForce)
            .set(ConstKeywords.KNOCKUP_FORCE,MyCurrentCombatData.KnockbackType!!.knockUpForce)
            .set(ConstKeywords.DAMAGE_COLORS,ColorList)
            .build()

        //set up the bleed for damage types
        val  BleedData =  Entity.newBuilder(taskKey).set(ConstKeywords.DAMAGE_MULTIPLIER,MyCurrentCombatData.BleedType!!.DamaveValues!!.DamageMultiplier)
            .set(ConstKeywords.BLEED_DEBUFF,MyCurrentCombatData.BleedType!!.BleedDebuffMultiplier)
            .set(ConstKeywords.BLEED_DURATION,MyCurrentCombatData.BleedType!!.BleedDuration)
            .set(ConstKeywords.DAMAGE_COLORS,ColorList)
            .build()

        //set up the Piercing for damage types
        val  PiercingData =  Entity.newBuilder(taskKey).set(ConstKeywords.DAMAGE_MULTIPLIER,MyCurrentCombatData.PiercingType!!.DamaveValues!!.DamageMultiplier)
            .set(ConstKeywords.PIERCING_DAMAGE,MyCurrentCombatData.PiercingType!!.PiercingDamage)
            .set(ConstKeywords.DAMAGE_COLORS,ColorList)
            .build()

        //set up the Stun for damage types
        val  StunData =  Entity.newBuilder(taskKey).set(ConstKeywords.DAMAGE_MULTIPLIER,MyCurrentCombatData.StunType!!.DamaveValues!!.DamageMultiplier)
            .set(ConstKeywords.STUN_DURATION,MyCurrentCombatData.StunType!!.StunDuration)
            .set(ConstKeywords.DAMAGE_COLORS,ColorList)
            .build()


        //prepares the new entity
        var data = Entity.newBuilder(taskKey).set(ConstKeywords.BASE_ENTITY,BaseData)
                                    .set(ConstKeywords.KNOCKBACK_ENTITY,KnockbackData)
                                    .set(ConstKeywords.BLEED_ENTITY,BleedData)
                                    .set(ConstKeywords.PIERCING_ENTITY,PiercingData)
                                    .set(ConstKeywords.STUN_ENTITY,StunData).build()

        //saves the entity
        datastore.put(data)



    }




}
