package com.example.combatdatacloudapp

import android.graphics.Color

//data class to store the damage multiplier for damage types
data class DamageValues(var DamageMultiplier:Double,var DamageColors:Color)


//data class to store the KnockBack Damage Type
data class Knockback(var DamaveValues:DamageValues?,
                     var knockBackForce:Double,
                     var knockUpForce:Double)

//data class to store the Bleed Damage Type
data class Bleed(var DamaveValues:DamageValues?,
                     var BleedDebuffMultiplier:Double,
                     var BleedDuration:Double)

//data class to store the Piercing Damage Type
data class Piercing(var DamaveValues:DamageValues?,
                 var PiercingDamage:Double)

//data class to store the Stun Damage Type
data class Stun(var DamaveValues:DamageValues?,
                    var StunDuration:Double)

// data class to store the combat data for unworthy roses
data class CombatData(var KeyName:String){
    var BaseType:DamageValues? = null
    var KnockbackType:Knockback? = null
    var BleedType:Bleed? = null
    var PiercingType:Piercing? = null
    var StunType:Stun? = null
}

//stores the Constant Keywords in the form of a Companion object so if we plan to change the name of any data later we can change it here and it will update throughout the project
class ConstKeywords(){
    companion object{
        val CREDENTIALS_NAME = "TelemetaryProject-802b6d5ef54b.json"
        val PROJECT_ID = "telemetaryproject"
        val ENTITY_KIND = "Kotlin Combat Data"
        val FORM_TITLE_KEY = "formTitle"
        val BASE_ENTITY = "BaseData"
        val KNOCKBACK_ENTITY = "KnockbackData"
        val BLEED_ENTITY = "BleedData"
        val PIERCING_ENTITY = "PiercingData"
        val STUN_ENTITY = "StunData"
        val DAMAGE_COLORS = "DamageColors"
        val DAMAGE_MULTIPLIER = "DamageMultiplier"
        val KNOCKUP_FORCE = "KnockupForce"
        val KNOCKBACK_FORCE = "KnockbackForce"
        val BLEED_DEBUFF = "BleedDebuff"
        val BLEED_DURATION = "BleedDuration"
        val PIERCING_DAMAGE = "PiercingDamage"
        val STUN_DURATION = "StunDuration"
    }
}

