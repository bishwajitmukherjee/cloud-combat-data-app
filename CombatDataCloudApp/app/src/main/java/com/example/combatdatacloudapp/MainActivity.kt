package com.example.combatdatacloudapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.datastore.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch



class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    //called whenever a Butotn is pressed to go to the combat data form.
    fun ButtonPressed(view: View)
    {
        val intent = Intent(this,CombatDataForm::class.java)
        intent.putExtra(ConstKeywords.FORM_TITLE_KEY,view.tag.toString())
        startActivity(intent)
    }
}

